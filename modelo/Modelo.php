<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author BlackBana
 */
interface Modelo {
    
    //metodos base de datos
    public function instalarBD();
    public function desinstalarBD();
    
    //metodos persona
    public function createPersona($persona);
    public function readPersona();
    public function idPersona();
    
    //metodos perro
    public function createPerro($perro);
    public function readPerro();
    public function idPerro();
    
}
