CREATE DATABASE ivanrodriguezcarrion;

USE ivanrodriguezcarrion;

CREATE TABLE personas (
		id INT AUTO_INCREMENT,
		nombre VARCHAR(25),
		apellidos VARCHAR(35),
		PRIMARY KEY (id));

CREATE TABLE perros (
		id INT AUTO_INCREMENT,
		nombre VARCHAR(25),
		raza VARCHAR(25),
		nChip INT,
		propietario INT,
		PRIMARY KEY (id),
		FOREIGN KEY (propietario) REFERENCES personas(id));

