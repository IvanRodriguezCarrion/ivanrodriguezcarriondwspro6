<?php

if(!isset($_SESSION)) {
    session_start();
}


error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once  __DIR__.'/Configuracion.php';
include_once  __DIR__.'/../modelo/ModeloMysql.php';
include_once  __DIR__.'/../modelo/ModeloFicheros.php';

function cabecera() {
    echo "<h1>" . Configuracion::$titulo . "</h1><hr/>\n";
}

function pie() {
    echo "<hr/><pre>" . Configuracion::$empresa . " " . Configuracion::$autor . " ";
    echo Configuracion::$curso . " " . Configuracion::$fecha . "</pre>\n";
}

function inicio() {
    echo "<align='right'><a href = '../index.php'>Inicio</a> </align>\n";
}

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
	$campo = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])));
    } else {
	$campo = "";
    };
    return $campo;
}

function comprobarModelo() {

    if ($_SESSION['modelo'] === "mysql") {
	$modeloElegido = new ModeloMysql();
    } else if ($_SESSION['modelo'] === "ficheros") {
	$modeloElegido = new ModeloFicheros();
    }
    return $modeloElegido;
}

?>