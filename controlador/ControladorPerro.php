<?php

function rellenarTablaPerro() {

    $modelo = comprobarModelo();
    foreach ($modelo->readPerro() as $r):
	?>
	<tr>
	    <td><?php echo $r->__GET('id'); ?></td>
	    <td><?php echo $r->__GET('nombre'); ?></td>
	    <td><?php echo $r->__GET('raza'); ?></td>
	    <td><?php echo $r->__GET('numChip'); ?></td>
	    <td><?php echo $r->__GET('propietario')->__GET('id'); ?></td>
	</tr>
	<?php
    endforeach;
}

function calcularIDPerros() {
    $modelo = comprobarModelo();
    $perros = $modelo->idPerro();
    echo $perros;
}
